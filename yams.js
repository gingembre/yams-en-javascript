window.onload = yamsGame;

function yamsGame () {

	let upperSecAccumulator = 0;
	let totalElm = getElm("totalScore");
	let messageNbrsTours=getElm("nb");
	messageNbrsTours.innerHTML="Pour lancer la partie cliquer sur lancer.";
	let match;
	let lancers = 0;
	let tableauBloquer = [false, false, false, false, false];//Permert de savoir si les dé doivent etre relancé
	let de = [getElm("0"), getElm("1"), getElm("2"), getElm("3"), getElm("4")];// Un tableau qui recupére l'ensemble des dé

	function getElm (id) {

		return document.getElementById(id);
	};

	function parseInnerHTML () { //sécurité pour que dans le tableau les case non cliqué soit égale a 0.

		if(this.innerHTML === "-") return 0;
		else return Number(this.innerHTML);
	};

	function total (value) {

		totalElm.innerHTML = Number(totalElm.innerHTML) + value;
	};

	//ici on traite le lancement des dés

	getElm("lancer").onclick = lancer;


		function lancer () {

			if(lancers < 3) {
				lancers++;
				for(let i = 0; i<5; i++) {

					if(tableauBloquer[i] === false || lancers === 0) {

						de[i].innerHTML = Math.floor(Math.random() * 6) + 1;
					}
				}
			}
			messageLancer()
		};

	function updateTableauBloquer (id) {

		for(let i = 0; i<5; i++) {

			tableauBloquer[i] = getElm("bloquer" + i).checked;

		}
	};


	function messageLancer(){
		let message;

		switch(lancers){
			case 0: message= "Vos lancers ont été remis a zero, vous pouvez relancer 3 fois.";
			break;
			case 1: message= "Encore 2 lancers.";
			break;
			case 2: message= "Vous n'avez plus que 1 lancer.";
			break;
			case 3: message= "Pour relancer, merci de séléctionner dans le tableau se que vous souhaitez bloquer :) ";
			break;
		}
		var messageModifie =getElm("nb");
		messageModifie.innerHTML = message;
	}

	function lancerRemettreAZero () {

		lancers = 0;

		for(let i = 0; i<5; i++) {

			de[i].innerHTML = 0;
		}
	};

	function disable (name) {

		let elm = getElm(name);
		elm.classList.add("strike");
		elm.classList.remove("cell");
		elm.onclick = undefined;
	};
	for(let i = 0; i<5; i++) {

		getElm("bloquer" + i).onclick = updateTableauBloquer;

	}

	function cellScore (name, value) { // sert a ajouter le score dans le tableau avec deux ergument id et la valeur que fait ressortir la fonction de calcul

		getElm(name + "Score").innerHTML = value;
		lancerRemettreAZero();
		disable(name);
		total(value);
		for(let i = 0; i<5; i++) {// remet a chaque click sur le tableau les check box decoché et le tableauBloquer a false

			getElm("bloquer"+i).checked= false;
			tableauBloquer[i] = false;
		}
		messageLancer();
	};

	function sumDe() {

		let accumulator = 0;

		for(let i = 0; i<5; i++) {

			accumulator += Number(de[i].innerHTML);
		}

		return accumulator;

	};

	//Fin dé
	//On commence ici les test du tableau du haut

	getElm("ones").onclick = hautDuTableau("ones", 1);
	getElm("twos").onclick = hautDuTableau("twos", 2);
	getElm("threes").onclick = hautDuTableau("threes", 3);
	getElm("fours").onclick = hautDuTableau("fours", 4);
	getElm("fives").onclick = hautDuTableau("fives", 5);
	getElm("sixes").onclick = hautDuTableau("sixes", 6);

	function upperSecScore (value) { //pour les 6 premieres cases du tableaucheck les dés et les accumulent.

		let accumulator = 0;

		for(let i = 0; i<5; i++) {

			if(Number(de[i].innerHTML) === value) {

				accumulator += value;
			}
		}

		return accumulator;
	};

	function hautDuTableau (name, value) {

		function func1 () {

			if(lancers > 0) {

				let upperScore = upperSecScore(value)
				upperSecAccumulator += upperScore;
				cellScore(name, upperScore);
				checkBonusTableauHaut();
			}
		};
		return func1;
	};

	//fin traitement click tableau du haut

	//Bonus de la section haute du tableau si 62 points ou plus de +35

	function checkBonusTableauHaut () {

		if(upperSecAccumulator > 62) cellScore("upperSecBonus", 35);
	};

	//fin traitement bonus tableau haut

	//brelan et carré début

	getElm("brelan").onclick = kind(3);
	getElm("carre").onclick = kind(4);
	function kind (kindVal) { // donc 3 pour les brelan et 4 pour les carré

		function func2 () {

			let score = 0;

			for(let i = 1; i<7; i++) {

				score = kindScore(i, kindVal) // kindVal = donc 3 pour les brelan et 4 pour les carré

				if(score) {

					if(kindVal === 3) {

						cellScore("brelan", score);

					} else {

						cellScore("carre", score);
					}
				}
			}
		};

		return func2;
	};

	function kindScore (value, kindVal) {

		let numbersOfAKind = 0;

		for(let i = 0; i<5; i++) {

			if(Number(de[i].innerHTML) === value) { // verifie pour chaque dé si il est egal a une valeur de 1 a 6 et si c'est le cas increment une variable

				numbersOfAKind += 1;
			}
		}

		if(numbersOfAKind >= kindVal) { // si il y en a 3 identiques pour un brelan = match et si 4 carré match

			return sumDe();

		} else {

			return undefined;
		}
	};

	//brelan et carré fin
	//début traitement du full

getElm("full").onclick = full;

	function full () {

		let brelanFound = 0;
		let paire = 0;

		for(let i = 1; i<7; i++) {

			if(kindScore(i, 3)) {

				brelanFound = i;
			}
		}

		for(let i = 1; i<7; i++) {

			if(brelanFound !== i && kindScore(i, 2)) {

				paire = i;
			}
		}

		if(brelanFound && paire) {

			cellScore("full", 25);
		}else if(lancers!=0){
			cellScore("full", 0);
		}
	};

	//fin du traitement du full
	//Début petite suite et grande suite

	getElm("petiteSuite").onclick = suite(true);
	getElm("grandeSuite").onclick = suite(false);

	function searchDe (val) { //Retourne true si les dés renvoi la valeur trouvé.

		let foundDe = false;

		for(let i = 0; i<5; i++) {

			if(Number(de[i].innerHTML) === val) {

				foundDe = true;
			}
		}

		return foundDe;
	};

	function checkSuiteCombo(search) {

		if(search[4] === undefined) { //verifie le combo de la petite suite

			if(searchDe(search[0]) &&
			searchDe(search[1]) &&
			searchDe(search[2]) &&
			searchDe(search[3]))

			return true;

		} else { //verifie le combo de la grande suite

			if(searchDe(search[0]) &&
			searchDe(search[1]) &&
			searchDe(search[2]) &&
			searchDe(search[3]) &&
			searchDe(search[4]))

			return true;
		}
	};

	function suite (smallOrLarge) { //boolean. True means small straight, false means large straight. Searches combinations of straights based on smallOrLarge boolean parameter.

		function func3 () {

			if(smallOrLarge) { //Small straight

				if(checkSuiteCombo([1, 2, 3, 4, undefined]) || checkSuiteCombo([2, 3, 4, 5, undefined]) || checkSuiteCombo([3, 4, 5, 6, undefined])) cellScore("petiteSuite", 30);

			} else { //Large straight

				if(checkSuiteCombo([1, 2, 3, 4, 5]) || checkSuiteCombo([2, 3, 4, 5, 6])) cellScore("grandeSuite", 40);
			}
		};

		return func3;
	};

	//traitement petite et grande uite fin.
	//case chance début

	getElm("chance").onclick = chance;

	function chance () {

		if(lancers > 0) cellScore("chance", sumDe());
	};

	//case chance fin
	//traitement du yams début

	getElm("yams").onclick = yams;

	function yams () {

		if(lancers > 0) {
			for(var i = 1, tempMatch = undefined, match = undefined; i<7; i++) {

							tempMatch = kindScore(i, 5);
							if(tempMatch !== undefined) match = true;
						}

			if(match) {

				cellScore("yams", 50);

			}else if(lancers!=0){
				cellScore("yams", 0);
			}
		}
	};

	//case yams fin

	getElm("recommencer").onclick = recommencer;

	function recommencer(){

			location.reload();
	}
};
